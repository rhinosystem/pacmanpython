# -*- coding: utf-8
#
# autores: Ronny Morán, Gustavo Londa
# Grupo #10|
# Lenguajes de Progamación I termino 2015
from pygame import *
import pygame
from pygame.locals import *
import random
import ctypes


#Arreglo para labererinto1
r1  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
r2  = [0,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,0,0]
r3  = [0,1,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,8,0,0,0,1,0,0,1,0,0]
r4  = [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0]
r5  = [0,1,0,0,2,0,0,0,0,0,0,1,0,1,0,2,0,2,0,2,0,0,2,0,0,2,0,0,1,0,0]
r6  = [0,1,0,0,1,0,2,1,1,0,0,1,0,1,0,2,0,1,0,1,0,0,1,0,0,1,0,0,1,0,0]
r7  = [0,1,0,0,1,0,2,1,1,0,0,1,0,1,0,2,0,1,0,1,0,0,1,0,0,1,0,0,1,0,0]
r8  = [0,1,0,0,1,1,2,1,1,1,1,1,2,1,1,2,0,1,1,1,1,1,1,0,0,1,0,0,1,0,0]
r9  = [0,1,0,0,2,0,2,2,0,0,0,0,2,2,1,2,2,2,0,0,0,0,1,1,1,1,1,1,1,0,0]
r10 = [0,1,1,1,1,0,2,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,2,0,1,0,0]
r11 = [0,1,0,0,1,0,2,1,1,2,0,5,5,5,5,5,5,5,2,0,1,0,1,0,0,0,2,0,1,0,0]
r12 = [0,1,2,2,1,0,2,1,1,2,0,2,2,1,1,2,2,2,2,0,1,0,2,0,0,0,2,0,1,0,0]
r13 = [0,1,0,0,0,0,0,0,0,1,0,5,5,4,5,5,5,5,5,0,1,0,1,1,1,1,1,1,1,0,0]
r14 = [0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0]
r15 = [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0]
r16 = [0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,1,0,2,1,0,0,0,0,0,1,0,0]
r17 = [0,1,1,1,1,0,2,0,0,1,0,0,0,0,0,0,0,1,0,1,0,2,1,1,1,1,1,1,1,0,0]
r18 = [0,1,0,0,0,0,2,0,0,1,1,1,1,1,4,1,1,1,1,1,0,2,0,0,0,0,0,0,1,0,0]
r19 = [1,1,1,1,1,1,2,1,1,1,0,0,0,1,1,0,0,1,1,1,1,2,1,1,1,1,1,1,1,1,1]
r20 = [0,1,0,0,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,2,0,0,0,0,0,0,1,0,0]
r21 = [0,1,0,0,1,1,2,1,0,0,0,0,1,0,0,0,0,1,0,1,0,2,1,1,1,1,1,1,1,0,0]
r22 = [0,1,0,0,1,0,0,1,0,0,0,0,1,0,0,0,0,1,0,1,0,2,0,0,0,0,0,0,1,0,0]
r23 = [0,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,0,0]
r24 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

r25 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,1,1,1,1,1,2,1,1,1,1,2,1,0,0,0]
r26 = [1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1,0,0,0]
r27 = [1,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,2,2,2,1,0,0,0]
r28 = [1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1,0,0,0]
r29 = [1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1,0,0,0]
r30 = [1,2,2,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,2,2,1,0,0,0]
r31 = [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1,0,0,0]
r32 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
r33 = [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1,0,0,0]
r34 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
r35 = [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1,0,0,0]
r36 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


#Arreglo para labererinto2
r37  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
r38  = [0,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,0,0]
r39  = [0,1,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,0,0]
r40  = [0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,4,1,1,1,0,0]
r41  = [0,1,2,0,2,0,0,0,0,0,2,0,2,1,0,2,0,2,0,2,0,2,2,2,0,2,0,0,1,0,0]
r42  = [0,1,1,0,1,0,2,1,1,0,0,1,0,1,0,2,1,1,0,1,0,0,1,0,0,1,0,0,1,0,0]
r43  = [0,1,1,0,1,0,2,1,1,0,0,1,2,1,1,2,1,1,1,1,0,2,1,0,0,1,0,0,1,0,0]
r44  = [0,1,1,0,1,1,2,1,1,0,0,1,2,0,1,2,1,1,1,0,0,0,1,0,0,1,0,0,1,0,0]
r45  = [0,1,2,0,2,0,2,2,2,0,0,0,2,2,0,2,2,2,2,0,2,2,2,2,0,2,1,0,1,0,0]
r46 = [0,1,1,1,1,0,2,1,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,2,0,1,0,0]
r47 = [0,1,1,1,0,0,2,1,1,2,1,1,1,1,1,1,1,1,2,0,1,0,1,0,0,1,2,0,1,0,0]
r48 = [0,1,2,2,2,0,2,1,1,2,2,2,2,1,1,2,2,2,2,0,1,0,2,0,2,2,2,1,1,0,0]
r49 = [0,1,0,0,0,0,2,0,0,1,1,1,0,4,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,0,0]
r50 = [0,1,0,0,0,0,0,0,0,1,1,1,0,1,1,0,1,1,1,1,1,2,1,0,0,0,0,0,1,0,0]
r51 = [0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,2,1,0,0,0,0,0,1,0,0]
r52 = [0,1,0,0,0,0,0,0,0,1,1,1,1,0,0,1,1,1,0,1,1,2,1,0,0,0,0,0,1,0,0]
r53 = [0,1,1,1,1,0,2,0,0,1,0,0,0,0,0,0,0,1,0,1,1,2,1,1,1,1,1,1,1,0,0]
r54 = [0,1,0,0,0,0,2,0,0,1,0,0,5,3,4,0,0,1,1,1,1,2,0,0,0,0,0,0,1,0,0]
r55 = [1,1,1,1,1,1,2,1,1,1,0,0,0,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1]
r56 = [0,1,0,0,0,1,2,1,1,0,1,1,1,1,1,1,1,1,0,1,1,2,1,0,0,0,0,0,1,0,0]
r57 = [0,1,0,0,0,1,2,1,1,0,0,1,1,0,0,0,0,0,0,1,1,2,1,0,0,0,0,0,1,0,0]
r58 = [0,1,0,0,0,1,2,1,1,0,1,1,1,1,0,1,0,1,0,1,1,2,1,0,0,0,0,0,1,0,0]
r59 = [0,4,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,4,0,0]
r60 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

r61 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,1,1,1,1,1,2,1,1,1,1,2,1,0,0,0]
r62 = [1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1,0,0,0]
r63 = [1,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,2,2,2,1,0,0,0]
r64 = [1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1,0,0,0]
r65 = [1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1,0,0,0]
r66 = [1,2,2,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,2,2,1,0,0,0]
r67 = [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1,0,0,0]
r68 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
r69 = [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1,0,0,0]
r70 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
r71 = [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1,0,0,0]
r72 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


rr1  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
rr2  = [0,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,0,0]
rr3  = [0,1,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,8,0,0,0,1,0,0,1,0,0]
rr4  = [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0]
rr5  = [0,1,0,0,2,0,0,0,0,0,0,1,0,1,0,2,0,2,0,2,0,0,2,0,0,2,0,0,1,0,0]
rr6  = [0,1,0,0,1,0,2,1,1,0,0,1,0,1,0,2,0,1,0,1,0,0,1,0,0,1,0,0,1,0,0]
rr7  = [0,1,0,0,1,0,2,1,1,0,0,1,0,1,0,2,0,1,0,1,0,0,1,0,0,1,0,0,1,0,0]
rr8  = [0,1,0,0,1,1,2,1,1,1,1,1,2,1,1,2,0,1,1,1,1,1,1,0,0,1,0,0,1,0,0]
rr9  = [0,1,0,0,2,0,2,2,0,0,0,0,2,2,1,2,2,2,0,0,0,0,1,1,1,1,1,1,1,0,0]
rr10 = [0,1,1,1,1,0,2,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,2,0,1,0,0]
rr11 = [0,1,0,0,1,0,2,1,1,2,0,5,5,5,5,5,5,5,2,0,1,0,1,0,0,0,2,0,1,0,0]
rr12 = [0,1,2,2,1,0,2,1,1,2,0,2,2,1,1,2,2,2,2,0,1,0,2,0,0,0,2,0,1,0,0]
rr13 = [0,1,0,0,0,0,0,0,0,1,0,5,5,4,5,5,5,5,5,0,1,0,1,1,1,1,1,1,1,0,0]
rr14 = [0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0]
rr15 = [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0]
rr16 = [0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,1,0,2,1,0,0,0,0,0,1,0,0]
rr17 = [0,1,1,1,1,0,2,0,0,1,0,0,0,0,0,0,0,1,0,1,0,2,1,1,1,1,1,1,1,0,0]
rr18 = [0,1,0,0,0,0,2,0,0,1,1,1,1,1,4,1,1,1,1,1,0,2,0,0,0,0,0,0,1,0,0]
rr19 = [1,1,1,1,1,1,2,1,1,1,0,0,0,1,1,0,0,1,1,1,1,2,1,1,1,1,1,1,1,1,1]
rr20 = [0,1,0,0,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,2,0,0,0,0,0,0,1,0,0]
rr21 = [0,1,0,0,1,1,2,1,0,0,0,0,1,0,0,0,0,1,0,1,0,2,1,1,1,1,1,1,1,0,0]
rr22 = [0,1,0,0,1,0,0,1,0,0,0,0,1,0,0,0,0,1,0,1,0,2,0,0,0,0,0,0,1,0,0]
rr23 = [0,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,0,0]
rr24 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

rr25 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,1,1,1,1,1,2,1,1,1,1,2,1,0,0,0]
rr26 = [1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1,0,0,0]
rr27 = [1,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,2,2,2,1,0,0,0]
rr28 = [1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1,0,0,0]
rr29 = [1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1,0,0,0]
rr30 = [1,2,2,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,2,2,1,0,0,0]
rr31 = [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1,0,0,0]
rr32 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
rr33 = [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1,0,0,0]
rr34 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
rr35 = [1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1,0,0,0]
rr36 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

laberinto = [r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20,r21,r22,r23,r24,r25,r26,r27,r28,r29,r30,r31,r32,r33,r34,r35,r36]
laberinto2 = [r37,r38,r39,r40,r41,r42,r43,r44,r45,r46,r47,r48,r49,r50,r51,r52,r53,r54,r55,r56,r57,r58,r59,r60,r61,r62,r63,r64,r65,r66,r67,r68,r69,r70,r71,r72]
laberinto3= [rr1,rr2,rr3,rr4,rr5,rr6,rr7,rr8,rr9,rr10,rr11,rr12,rr13,rr14,rr15,rr16,rr17,rr18,rr19,rr20,rr21,rr22,rr23,rr24,rr25,rr26,rr27,rr28,rr29,rr30,rr31,rr32,rr33,rr34,rr35,rr36]

pygame.init()






class Opcion:

    def __init__(self, tipo_de_letra, tittle, x, y, parid, func_asig):
        self.img_norm = tipo_de_letra.render(tittle, 1, (0, 0, 0))
        self.img_posi = tipo_de_letra.render(tittle, 1, (200, 0, 0))
        self.image = self.img_norm
        self.rect = self.image.get_rect()
        self.rect.x = 500 * parid
        self.rect.y = y
        self.func_asig = func_asig
        self.x = float(self.rect.x)

    def refresh(self):
        destino_x = 105
        self.x += (destino_x - self.x) / 5.0
        self.rect.x = int(self.x)

    def print_pantalla(self, pantalla_menu):
        pantalla_menu.blit(self.image, self.rect)

    def destacar(self, estado):
        if estado:
            self.image = self.img_posi
        else:
            self.image = self.img_norm

    def bandera(self):
        self.func_asig()


class Cursor:

    def __init__(self, x, y, dy):
        self.image = pygame.image.load('cursor.png').convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.y_inicial = y
        self.dy = dy
        self.y = 0
        self.seleccionar(0)

    def refresh(self):
        self.y += (self.to_y - self.y) / 10.0
        self.rect.y = int(self.y)

    def seleccionar(self, indice):
        self.to_y = self.y_inicial + indice * self.dy

    def print_pantalla(self, pantalla_menu):
        pantalla_menu.blit(self.image, self.rect)


class Menu:
    "Representa un menú con opt para un juego"

    def __init__(self, opt):
        self.opt = []
        tipo_de_letra = pygame.font.Font('dejavu.ttf', 20)
        x = 105
        y = 105
        parid = 1

        self.cursor = Cursor(x - 30, y, 30)

        for tittle, funcion in opt:
            self.opt.append(Opcion(tipo_de_letra, tittle, x, y, parid, funcion))
            y += 30
            if parid == 1:
                parid = -1
            else:
                parid = 1

        self.seleccionado = 0
        self.total = len(self.opt)
        self.pulsada = False

    def refresh(self):
        """Altera el valor de 'self.seleccionado' con los direccionales."""

        k = pygame.key.get_pressed()

        if not self.pulsada:
            if k[K_UP]:
                self.seleccionado -= 1
            elif k[K_DOWN]:
                self.seleccionado += 1
            elif k[K_RETURN]:
                # Invoca a la función asociada a la opción.
                self.opt[self.seleccionado].bandera()

        # procura que el cursor esté entre las opt permitidas
        if self.seleccionado < 0:
            self.seleccionado = 0
        elif self.seleccionado > self.total - 1:
            self.seleccionado = self.total - 1

        self.cursor.seleccionar(self.seleccionado)

        # indica si el usuario mantiene pulsada alguna tecla.
        self.pulsada = k[K_UP] or k[K_DOWN] or k[K_RETURN]

        self.cursor.refresh()

        for o in self.opt:
            o.refresh()

    def print_pantalla(self, pantalla_menu):
        """Imprime sobre 'pantalla_menu' el texto de cada opción del menú."""

        self.cursor.print_pantalla(pantalla_menu)

        for opcion in self.opt:
            opcion.print_pantalla(pantalla_menu)

			
#funcion para iniciar el juego pacman
def iniciar_juego():
    done=True
    pacman= Pacman(120,120)
    fantasmaA=Fantasma(450,180)
    fantasmaB=Fantasma(450,180)
    fantasmaC=Fantasma(450,180)
    fantasmaD=Fantasma(450,180)
    imagenFantasma=fantasmaA.getImagen()
    imagenFantasma2=fantasmaB.getImagen()
    imagenFantasma3=fantasmaC.getImagen()
    imagenFantasma4=fantasmaD.getImagen()
    reloj = pygame.time.Clock()
    contarT=0
    pantalla = pygame.display.set_mode((1020,720),0,32)
    imagen = pygame.image.load("pacman.png")
    rock = pygame.image.load("roca2.bmp")
    rock2 = pygame.image.load("roca.bmp")


    comida = pygame.image.load("pgomme.png")
    fruta = pygame.image.load("fruta.png")
    pygame.display.set_caption("Pacman")

    fuente = pygame.font.Font(None, 20)
    comidaCount = 0
    puntaje = 0
    vidas=3
    nivel=1
    nivelCom=1
    while done:

            if vidas==0:
                done = False
                ctypes.windll.user32.MessageBoxA(0, "GAME OVER", "GAME OVER", 1)
                pantalla = pygame.display.set_mode((320,240),0,32)
                for ie in range(29):
                            for je in range(30):
                                laberinto[ie][je]= laberinto3[ie][je]




            for eventos in pygame.event.get():
                if eventos.type == pygame.QUIT:
                    exit()
                elif eventos.type == KEYDOWN:
                    if eventos.key == K_ESCAPE:
                        done = False
                        pantalla = pygame.display.set_mode((320,240),0,32)
                        for ie in range(29):
                            for je in range(30):
                                laberinto[ie][je]= laberinto3[ie][je]

            if puntaje==3350:
                puntaje=puntaje+10

                nivel=nivel+1
                nivelCom=nivelCom+1
                pacman.setY(120)
                pacman.setX(120)
                fantasmaA.setRojo(True)
                fantasmaB.setRojo(True)
                fantasmaC.setRojo(True)
                fantasmaD.setRojo(True)
                fantasmaA.setX(450)
                fantasmaA.setY(180)
                fantasmaB.setX(450)
                fantasmaB.setY(180)
                fantasmaC.setX(450)
                fantasmaC.setY(180)
                fantasmaD.setX(450)
                fantasmaD.setY(180)
                
            if nivelCom==2:
		
					
                nivelCom=0
                nivel=2
                rock=rock2
                for ii in range(29):
                    for jj in range(30):
                        laberinto[ii][jj]= laberinto2[ii][jj]

            pulsada = pygame.key.get_pressed()
            if pulsada[K_F1]:
                pygame.display.set_mode((1120,720),pygame.FULLSCREEN)
            if pulsada[K_UP]:
                 if laberinto[(pacman.getY()-30)/30][pacman.getX()/30]!=0:
                    if laberinto[(pacman.getY()-30)/30][pacman.getX()/30]==1:
                        laberinto[(pacman.getY()-30)/30][pacman.getX()/30]=3
                        comidaCount=comidaCount+1
                        if fantasmaA.getRojo() and fantasmaB.getRojo() and fantasmaC.getRojo() and fantasmaD.getRojo():
                           pygame.mixer.music.load('pacman_chomp.wav')
                           pygame.mixer.music.play(0)
                    if laberinto[(pacman.getY()-30)/30][pacman.getX()/30]==4:
                        laberinto[(pacman.getY()-30)/30][pacman.getX()/30]=3
                        comidaCount=comidaCount+5
                        imagenFantasma2=pygame.image.load("fantasmaA.png")
                        fantasmaA.setImagen(imagenFantasma2)
                        fantasmaB.setImagen(imagenFantasma2)
                        fantasmaC.setImagen(imagenFantasma2)
                        fantasmaD.setImagen(imagenFantasma2)
                        fantasmaA.setRojo(False)
                        fantasmaB.setRojo(False)
                        fantasmaC.setRojo(False)
                        fantasmaD.setRojo(False)


                        pygame.mixer.music.load('pacman_intermission.wav')
                        pygame.mixer.music.play(0)
                    imagen=pacman.moverArriba()


            if pulsada[K_DOWN]:
                if laberinto[(pacman.getY()+30)/30][pacman.getX()/30]!=0:
                    if laberinto[(pacman.getY()+30)/30][pacman.getX()/30]==1:
                        laberinto[(pacman.getY()+30)/30][pacman.getX()/30]=3
                        comidaCount=comidaCount+1
                        if fantasmaA.getRojo() and fantasmaB.getRojo() and fantasmaC.getRojo() and fantasmaD.getRojo():
                           pygame.mixer.music.load('pacman_chomp.wav')
                           pygame.mixer.music.play(0)
                    if laberinto[(pacman.getY()+30)/30][pacman.getX()/30]==4:
                        laberinto[(pacman.getY()+30)/30][pacman.getX()/30]=3
                        comidaCount=comidaCount+5
                        imagenFantasma2=pygame.image.load("fantasmaA.png")
                        fantasmaA.setImagen(imagenFantasma2)
                        fantasmaB.setImagen(imagenFantasma2)
                        fantasmaC.setImagen(imagenFantasma2)
                        fantasmaD.setImagen(imagenFantasma2)
                        fantasmaA.setRojo(False)
                        fantasmaB.setRojo(False)
                        fantasmaC.setRojo(False)
                        fantasmaD.setRojo(False)

                        pygame.mixer.music.load('pacman_intermission.wav')
                        pygame.mixer.music.play(0)
                    imagen=pacman.moverAbajo()
            if pulsada[K_LEFT]:
                if laberinto[pacman.getY()/30][(pacman.getX()-30)/30]!=0:
                    if laberinto[pacman.getY()/30][(pacman.getX()-30)/30]==1:
                        laberinto[pacman.getY()/30][(pacman.getX()-30)/30]=3
                        comidaCount=comidaCount+1
                        if fantasmaA.getRojo() and fantasmaB.getRojo() and fantasmaC.getRojo() and fantasmaD.getRojo():
                           pygame.mixer.music.load('pacman_chomp.wav')
                           pygame.mixer.music.play(0)
                    if laberinto[pacman.getY()/30][(pacman.getX()-30)/30]==4:
                        laberinto[pacman.getY()/30][(pacman.getX()-30)/30]=3
                        comidaCount=comidaCount+5
                        imagenFantasma2=pygame.image.load("fantasmaA.png")
                        fantasmaA.setImagen(imagenFantasma2)
                        fantasmaB.setImagen(imagenFantasma2)
                        fantasmaC.setImagen(imagenFantasma2)
                        fantasmaD.setImagen(imagenFantasma2)
                        fantasmaA.setRojo(False)
                        fantasmaB.setRojo(False)
                        fantasmaC.setRojo(False)
                        fantasmaD.setRojo(False)

                        pygame.mixer.music.load('pacman_intermission.wav')
                        pygame.mixer.music.play(0)
                    imagen=pacman.moverIzquierda()
            if pulsada[K_RIGHT]:
                 if laberinto[pacman.getY()/30][(pacman.getX()+30)/30]!=0:
                    if laberinto[pacman.getY()/30][(pacman.getX()+30)/30]==1:
                        laberinto[pacman.getY()/30][(pacman.getX()+30)/30]=3
                        comidaCount=comidaCount+1
                        if fantasmaA.getRojo() and fantasmaB.getRojo() and fantasmaC.getRojo() and fantasmaD.getRojo():
                           pygame.mixer.music.load('pacman_chomp.wav')
                           pygame.mixer.music.play(0)
                    if laberinto[pacman.getY()/30][(pacman.getX()+30)/30]==4:
                        laberinto[pacman.getY()/30][(pacman.getX()+30)/30]=3
                        comidaCount=comidaCount+5
                        imagenFantasma2=pygame.image.load("fantasmaA.png")
                        fantasmaA.setImagen(imagenFantasma2)
                        fantasmaB.setImagen(imagenFantasma2)
                        fantasmaC.setImagen(imagenFantasma2)
                        fantasmaD.setImagen(imagenFantasma2)
                        fantasmaA.setRojo(False)
                        fantasmaB.setRojo(False)
                        fantasmaC.setRojo(False)
                        fantasmaD.setRojo(False)
                        pygame.mixer.music.load('pacman_intermission.wav')
                        pygame.mixer.music.play(0)
                    imagen=pacman.moverDerecha()

            reloj.tick(500)
            pantalla.fill((0,0,0))
            for i in range(29):
                    for j in range(30):
                        c= laberinto[i][j]
                        if c==0:
                           pantalla.blit(rock,(j*30,i*30))
                        if c==1:
                           pantalla.blit(comida,(j*30,i*30))
                        if c==4:
                           pantalla.blit(fruta,(j*30,i*30))
            #text = "X:"+ str(pacman.getX())+"  Y:"+ str(pacman.getY())
            #mensaje = fuente.render(text, 1, (255, 255, 255))
            #pantalla.blit(mensaje,(920,120))
            puntaje=comidaCount*10
            text2 = "Puntaje: "+ str(puntaje)
            mensaje2 = fuente.render(text2, 1, (255, 255, 255))
            pantalla.blit(mensaje2,(920,140))
            pantalla.blit(imagen,(pacman.getX(),pacman.getY()))

            rad=random.randint(1,10)
            if rad==1:
                imagenFantasma=fantasmaA.moverArriba()
            if rad==2:
                imagenFantasma=fantasmaA.moverAbajo()
            if rad==3:
                imagenFantasma=fantasmaA.moverDerecha()
            if rad==4:
               imagenFantasma=fantasmaA.moverIzquierda()

            '''Pacman cocha con el fantasma A'''
            if fantasmaA.getX()==pacman.getX() and fantasmaA.getY()==pacman.getY():
                if fantasmaA.getRojo():
                    vidas=vidas-1
                    pygame.mixer.music.load('pacman_death.wav')
                    pygame.mixer.music.play(0)
                    imagenG = pygame.image.load("pacman4.png")
                    pacman.setImagen(imagenG)
                    pacman.setX(120)
                    pacman.setY(120)
                else:
                    fantasmaA.setX(450)
                    fantasmaA.setY(180)
                    fantasmaA.setRojo(True)
                    imagenFantasma2=pygame.image.load("fantasmaR.png")
                    fantasmaA.setImagen(imagenFantasma2)

            '''Pacman cocha con el fantasma B'''
            if fantasmaB.getX()==pacman.getX() and fantasmaB.getY()==pacman.getY():
                if fantasmaB.getRojo():
                    vidas=vidas-1
                    pygame.mixer.music.load('pacman_death.wav')
                    pygame.mixer.music.play(0)
                    imagenG = pygame.image.load("pacman4.png")
                    pacman.setImagen(imagenG)

                    pacman.setX(120)
                    pacman.setY(120)
                else:
                    fantasmaB.setX(450)
                    fantasmaB.setY(180)
                    fantasmaB.setRojo(True)
                    imagenFantasma2=pygame.image.load("fantasmaR.png")
                    fantasmaB.setImagen(imagenFantasma2)
            '''Pacman cocha con el fantasma C'''
            if fantasmaC.getX()==pacman.getX() and fantasmaC.getY()==pacman.getY():
                if fantasmaC.getRojo():
                    vidas=vidas-1
                    pygame.mixer.music.load('pacman_death.wav')
                    pygame.mixer.music.play(0)
                    imagenG = pygame.image.load("pacman4.png")
                    pacman.setImagen(imagenG)
                    pacman.setX(120)
                    pacman.setY(120)
                else:
                    fantasmaC.setX(450)
                    fantasmaC.setY(180)
                    fantasmaC.setRojo(True)
                    imagenFantasma2=pygame.image.load("fantasmaR.png")
                    fantasmaC.setImagen(imagenFantasma2)
            '''Pacman cocha con el fantasma D'''
            if fantasmaD.getX()==pacman.getX() and fantasmaD.getY()==pacman.getY():
                if fantasmaD.getRojo():
                    vidas=vidas-1
                    pygame.mixer.music.load('pacman_death.wav')
                    pygame.mixer.music.play(0)
                    imagenG = pygame.image.load("pacman4.png")
                    pacman.setImagen(imagenG)
                    pacman.setX(120)
                    pacman.setY(120)
                else:
                    fantasmaD.setX(450)
                    fantasmaD.setY(180)
                    fantasmaD.setRojo(True)
                    imagenFantasma2=pygame.image.load("fantasmaR.png")
                    fantasmaD.setImagen(imagenFantasma2)
            pantalla.blit(imagenFantasma,(fantasmaA.getX(),fantasmaA.getY()))
            rad=random.randint(1,10)
            if rad==1:
                imagenFantasma2=fantasmaB.moverArriba()
            if rad==2:
                imagenFantasma2=fantasmaB.moverAbajo()
            if rad==3:
                imagenFantasma2=fantasmaB.moverDerecha()
            if rad==4:
               imagenFantasma2=fantasmaB.moverIzquierda()
            pantalla.blit(imagenFantasma2,(fantasmaB.getX(),fantasmaB.getY()))

            rad=random.randint(1,10)
            if rad==1:
                imagenFantasma3=fantasmaC.moverArriba()
            if rad==2:
                imagenFantasma3=fantasmaC.moverAbajo()
            if rad==3:
                imagenFantasma3=fantasmaC.moverDerecha()
            if rad==4:
               imagenFantasma3=fantasmaC.moverIzquierda()
            pantalla.blit(imagenFantasma3,(fantasmaC.getX(),fantasmaC.getY()))

            rad=random.randint(1,10)
            if rad==1:
                imagenFantasma4=fantasmaD.moverArriba()
            if rad==2:
                imagenFantasma4=fantasmaD.moverAbajo()
            if rad==3:
                imagenFantasma4=fantasmaD.moverDerecha()
            if rad==4:
               imagenFantasma=fantasmaD.moverIzquierda()
            if fantasmaA.getRojo() and fantasmaB.getRojo() and fantasmaC.getRojo() and fantasmaD.getRojo():
               contarT=0
            else:
                contarT=contarT+1
            if contarT==210:
                imagenFantasma2=pygame.image.load("fantasmaR.png")
                fantasmaA.setImagen(imagenFantasma2)
                fantasmaB.setImagen(imagenFantasma2)
                fantasmaC.setImagen(imagenFantasma2)
                fantasmaD.setImagen(imagenFantasma2)
                fantasmaA.setRojo(True)
                fantasmaB.setRojo(True)
                fantasmaC.setRojo(True)
                fantasmaD.setRojo(True)
            pantalla.blit(imagenFantasma4,(fantasmaD.getX(),fantasmaD.getY()))
            text2 = "Vidas: "+ str(vidas)
            mensaje2 = fuente.render(text2, 1, (255, 255, 255))
            pantalla.blit(mensaje2,(920,100))
            text2 = "Nivel: "+ str(nivel)
            mensaje2 = fuente.render(text2, 1, (255, 255, 255))
            pantalla.blit(mensaje2,(920,180))
            pantalla.blit(imagen,(pacman.getX(),pacman.getY()))
            pygame.display.flip()
def salir_del_programa():
    import sys
    print " Gracias por utilizar este programa."
    sys.exit(0)

	

#Clase pacman
class Pacman(object):
    x = 0
    y = 0
    imagen = pygame.image.load("pacman.png")

    # The class "constructor" - It's actually an initializer
    def __init__(self, x , y):
        self.x = x
        self.y = y
        pygame.mixer.music.load('pacman_beginning.wav')
        pygame.mixer.music.play(-1)
    def getY(self):
        return self.y
    def setY(self,y):
        self.y=y
    def getX(self):
        return self.x
    def setX(self,x):
        self.x=x
    def setImagen(self,imagen):
        self.imagen=imagen
    def moverArriba(self):
        self.y-=30
        self.imagen = pygame.image.load("pacman3.png")
        return self.imagen
    def moverAbajo(self):
        self.y+=30
        self.imagen = pygame.image.load("pacman4.png")
        return self.imagen
    def moverDerecha(self):
        if (self.x==870) and (self.y==540):
            self.x=-30
            self.y=540
        self.x+=30
        self.imagen = pygame.image.load("pacman.png")
        return self.imagen
    def moverIzquierda(self):
        if (self.x==-30) and (self.y==540):
            self.x=900
            self.y=540
        self.x-=30
        self.imagen = pygame.image.load("pacman2.png")
        return self.imagen
#clase Fantasma
class Fantasma(object):
    x = 0
    y = 0
    rojo=True
    imagen= pygame.image.load("fantasmaR.png")

    # The class "constructor" - It's actually an initializer
    def __init__(self, x , y):
        self.x = x
        self.y = y
    def getY(self):
        return self.y
    def getX(self):
        return self.x
    def setY(self, y):
        self.y=y
    def setX(self, x):
        self.x=x
    def getRojo(self):
        return self.rojo
    def setRojo(self, rojo):
        self.rojo=rojo
    def getImagen(self):
        return self.imagen
    def setImagen(self,imagen):
        self.imagen=imagen
    def moverArriba(self):
        if laberinto[(self.getY()-30)/30][self.getX()/30]!=0:
            self.y-=30
        return self.imagen
    def moverAbajo(self):
        if laberinto[(self.getY()+30)/30][self.getX()/30]!=0:
            self.y+=30
        return self.imagen
    def moverDerecha(self):
        if laberinto[(self.getY())/30][(self.getX()+30)/30]!=0:
            if (self.x==870) and (self.y==540):
                self.x=-30
                self.y=540
            self.x+=30
        return self.imagen
    def moverIzquierda(self):
        if laberinto[(self.getY())/30][(self.getX()-30)/30]!=0:
            if (self.x==-30) and (self.y==540):
                self.x=900
                self.y=540
            self.x-=30
        return self.imagen



#menu del juego
if __name__ == '__main__':

    salir = False
    opt = [
        ("Comenzar", iniciar_juego),

        ("Salir", salir_del_programa)
        ]

    pygame.font.init()
    pantalla_menu = pygame.display.set_mode((320, 240))
    backgroud_menu = pygame.image.load("menu_fond.png").convert()
    menu = Menu(opt)

    while not salir:

        for e in pygame.event.get():
            if e.type == QUIT:
                salir = True

        pantalla_menu.blit(backgroud_menu, (0, 0))
        menu.refresh()
        menu.print_pantalla(pantalla_menu)

        pygame.display.flip()
        pygame.time.delay(10)









